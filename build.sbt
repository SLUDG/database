name := "database"
organization := "com.sludg.util"
version := "0.1"
scalaVersion := "2.13.1"
crossScalaVersions := Seq("2.12.10", "2.11.12")

resolvers += "SLUDG@GitLab" at "https://gitlab.com/api/v4/groups/SLUDG/-/packages/maven"

val postgresDependencies = Seq(
  "com.github.tminglei" %% "slick-pg" % "0.18.0",
  "com.github.tminglei" %% "slick-pg_play-json" % "0.18.0",
  "org.postgresql" % "postgresql" % "42.2.6"
)

libraryDependencies ++= postgresDependencies


enablePlugins(GitlabPlugin)
com.sludg.sbt.gitlab.GitlabPlugin.gitlabProjectId := "14458655"